<?php
	$pid=$_GET["pid"];
	$cname=$_GET["cname"];
	$imgfolder='ProductImages/';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Content-Script-Type" content="text/javascript">
		<meta http-equiv="Content-Style-Type" content="text/css">
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	</head>
	<body>
		
		<div class="row">
		  <div class="col-xs-6 col-md-4"></div>
		  <div class="col-xs-6 col-md-4">
		  	<form name="upload" method="post" action="upload_process.php<?php echo "?cname=".$cname."&pid=".$pid;?>" enctype="multipart/form-data">
			  <div class="form-group">
			    <label for="InputFile">上傳檔案(品號:<?php echo $pid?>)</label>
			    <input type="file" id="InputFile" name="files[]" multiple/>
			    <p class="help-block">請上傳圖片檔案</p>
			  </div>
			  <?php
				  if(is_null($pid)||$pid==""){echo '<font color="red">沒有品號就不能上傳了！</font>';}else{echo @'<button type="submit" class="btn btn-default">開始上傳</button>';}
				  ?>
		    </form>
		  </div>
		  <div class="col-xs-6 col-md-4"></div>
		</div>

	</body>
</html>	
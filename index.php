<?php
	$pid=$_GET["pid"];
	$cname=$_GET["cname"].'/';
	$imgfolder='ProductImages/';
	?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">	
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<style type="text/css">
	.footerMessage {
		font-size: 10pt;
		color: #B2B2B2;
	}
	.showbox {
		width: 500px;
		border: 0px;
		vertical-align: middle;
	}
	.showbox img {
		width:inherit;
		height:inherit;
		overflow:hidden;
	}
	.abgne-block-20120106 {
		margin-top: 10px;
		width: 600px;
		overflow: hidden;
	}
	.abgne-block-20120106 a {
		margin-right: 10px;
	}
	.abgne-block-20120106 a img {
		width: 140px;
		height: 92px;
		border: 0px;
		vertical-align: middle;
	}
</style>
<script type="text/javascript">
	$(function(){
		// 用來顯示大圖片用
		var $showImage = $('#show-image');
		
		// 當滑鼠移到 .abgne-block-20120106 中的某一個超連結時
		$('.abgne-block-20120106 a').mouseover(function(){
			var $this = $(this), 
				_src = $this.attr('href');
			// 如果現在大圖跟目前要顯示的圖不是同一張時
			if($showImage.attr('src') != _src){
				$showImage.hide().attr('src', _src).stop(false, true).fadeTo(400, 1);
			}
		});
	});
</script>
</head>
<body>
	<?php 
		$filenames=$imgfolder.$cname.$pid."*";
		$get404=false;
		if(file_exists(current(glob($filenames))))
		{
			$showboximage=current(glob($filenames));
		}else
		{
			$showboximage="img/404.jpg";
			$get404=true;
		}
		?>

	<div class="showbox"><img id="show-image" src=<?php echo $showboximage;?> width="500px" /></div>
	
	<div class="abgne-block-20120106">
	<?php 
	foreach(glob($filenames) as $filename){
		echo "<a href=".$filename." target='_blank'><img src=".$filename." /></a>";
	}
	?>
	</div>
	<p></p>
	<?php if(!$get404){
		echo @"<div class='footerMessage'>小提示：點小圖開大圖喔！</div>";
	}
	?>
</body>